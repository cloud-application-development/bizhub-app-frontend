import axios from 'axios';
import firebase from './fireinit';

const API = axios.create({
  baseURL: '/api',
});

API.interceptors.request.use(async (config) => {
  if (firebase.auth().currentUser != null) {
    const currentUser = await firebase.auth().currentUser;
    config.headers.token = await currentUser.getIdToken();
  }
  return config;
}, (error) => Promise.reject(error));

export default API;
