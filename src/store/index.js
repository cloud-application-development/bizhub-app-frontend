import { createStore } from 'vuex';
import router from '../router/index';
import firebase from '../fireinit';
import api from '../API';

export default createStore({
  state: {
    user: {
      admin: false,
      displayName: '',
      email: '',
      name: '',
      surname: '',
      tenantId: '',
      userId: '',
      photoURL: '',
      groups: [],
    },
    error: null,
    darkModeActive: false,
    isFreeTier: false,
    isPremiumTier: false,
    isEnterpriseTier: false,
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    isDarkModeActive(state) {
      state.darkModeActive = JSON.parse(localStorage.getItem('darkModeActive'));
      return state.darkModeActive;
    },
    isEnterpriseTier(state) {
      return state.isEnterpriseTier;
    },
    isPremiumTier(state) {
      return state.isPremiumTier;
    },
    isFreeTier(state) {
      return state.isFreeTier;
    },
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload;
    },
    setUserGroups(state, payload) {
      state.user.groups = payload;
    },
    setError(state, payload) {
      state.error = payload;
    },
    setFreeTier(state, payLoad) {
      state.isFreeTier = payLoad;
    },
    setPremiumTier(state, payLoad) {
      state.isPremiumTier = payLoad;
    },
    setEnterpriseTier(state, payLoad) {
      state.isEnterpriseTier = payLoad;
    },
    setDarkModeActive(state, payLoad) {
      state.darkModeActive = payLoad;
      localStorage.setItem('darkModeActive', payLoad);
    },
  },
  actions: {
    authAction({ commit }) {
      firebase.auth().onAuthStateChanged(async (user) => {
        if (user) {
          const additionalUserData = await api.get(`/users/${user.uid}`);
          const userCredential = JSON.parse(JSON.stringify({
            userId: user.uid,
            displayName: user.displayName,
            tenantId: user.tenantId,
            email: user.email,
            photoURL: additionalUserData.data.photoURL,
            admin: additionalUserData.data.admin,
            name: additionalUserData.data.name,
            surname: additionalUserData.data.surname,
            groups: additionalUserData.data.groups,
          }));
          commit('setUser', userCredential);
          const additionalTenantData = await api.get(`/tenants/${user.tenantId}`).then((response) => response.data);
          const { tier } = additionalTenantData;
          switch (tier.toLowerCase()) {
            case 'premium':
              commit('setPremiumTier', true);
              break;
            case 'enterprise':
              commit('setEnterpriseTier', true);
              break;
            default:
              commit('setFreeTier', true);
          }
        } else {
          commit('setUser', null);
        }
      });
    },
    async signUpAction({ commit }, payload) {
      await api.post('/tenants', {
        tenantName: payload.tenantName,
        tier: payload.tier,
        userObject: {
          name: payload.name,
          surname: payload.surname,
          photoURL: '',
          email: payload.email,
          password: payload.password,
        },
      })
        .then((response) => {
          router.push({ name: 'LoginTenant', params: { id: response.data.tenantId } });
        })
        .catch((error) => {
          commit('setError', error.message);
        });
    },
    signInAction({ commit }, payload) {
      firebase.auth().tenantId = payload.tenantId;
      firebase
        .auth()
        .signInWithEmailAndPassword(payload.email, payload.password).then((user) => {
          router.push({ name: 'Wall' });
        })
        .catch((error) => {
          commit('setError', error.message);
        });
    },
    signOutAction({ commit }) {
      firebase
        .auth()
        .signOut()
        .then(() => {
          commit('setUser', null);
          router.go('/');
        })
        .catch((error) => {
          commit('setError', error.message);
        });
    },
    async changeUserInformationAction({ commit }, payload) {
      await api.put(`/users/${payload.usert.userId}`, {
        // übermittle neue user informationen
        name: payload.usert.name === null ? null : payload.usert.name,
        surname: payload.usert.surname === null ? null : payload.usert.surname,
        displayName: payload.usert.displayName === null ? null : payload.usert.displayName,
        email: payload.usert.email === null ? null : payload.usert.email,
        admin: payload.usert.admin ? payload.usert.admin : null,
        rawImage: payload.rawImage ? payload.rawImage : null,
      }).then((result) => {
        console.log(result);
      }).catch((error) => {
        console.log(error);
      });
    },
  },
  modules: {
  },
});
