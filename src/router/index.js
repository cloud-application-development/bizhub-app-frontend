import { createRouter, createWebHistory } from 'vue-router';
import firebase from '../fireinit';

const routes = [
  {
    path: '/',
    redirect: '/wall',
  },
  {
    path: '/wall',
    name: 'Wall',
    meta: {
      requiresAuth: true,
    },
    component: () => import('../views/Wall.vue'),
  },
  {
    path: '/wall/groups/:groupId',
    name: 'Group Wall',
    meta: {
      requiresAuth: true,
    },
    component: () => import('../views/Wall.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    // redirect: '/register',
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/login/:id',
    name: 'LoginTenant',
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue'),
  },
  {
    path: '/groups',
    name: 'groups',
    component: () => import(/* webpackChunkName: "groups" */ '../views/Groups.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/createUser',
    name: 'create-user',
    meta: {
      requiresAuth: true,
    },
    component: () => import('../views/CreateUser.vue'),
  },
  {
    path: '/showUser',
    name: 'show-user',
    meta: {
      requiresAuth: true,
    },
    component: () => import('../views/ShowUser.vue'),
  },
  {
    path: '/bizhub',
    name: 'bizhub',
    component: () => import('../views/LandingPage.vue'),
  },
];

const router = createRouter(
  {
    history: createWebHistory(),
    routes,
  },
);

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  if (requiresAuth && !await firebase.getCurrentUser()) {
    next('login');
  } else {
    next();
  }
});

export default router;
