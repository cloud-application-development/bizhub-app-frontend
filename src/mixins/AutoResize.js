export default {
  methods: {
    mixin_autoResize_resize(event) {
      const newEvent = event;
      newEvent.target.style.height = 'auto';
      newEvent.target.style.height = `${event.target.scrollHeight}px`;
    },
  },
  mounted() {
    this.$nextTick(() => {
      this.$el.setAttribute(
        'style',
        'height',
        `${this.$el.scrollHeight}px`,
      );
    });
  },
};
