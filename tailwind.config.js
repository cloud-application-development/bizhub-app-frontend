const typography = require('@tailwindcss/typography');

module.exports = {
  purge: ['./public/**/*.html', './src/**/*.vue'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      spacing: {
        16: '4em',
        20: '5em',
        24: '6em',
        28: '7em',
      },
      colors: {
        'white-opposite': '#202323',
        'gray-100-opposite': '#313535',
        'gray-200-opposite': '#414444',
        'gray-500-opposite': '#dfceb5',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    typography,
  ],
};
